var mongoose = require("mongoose");

var adSchema = new mongoose.Schema({
    name : String,
    type : String,
    region : String,
    language : String
});

module.exports = mongoose.model("Ad",adSchema);