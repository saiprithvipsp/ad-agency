var express = require("express"),
    bodyParser = require("body-parser"),
    mongoose = require("mongoose");
    app = express();
    Ad = require("./models/ad");
    seedDB = require("./seedDB")
    mongoose.connect("mongodb://localhost:27017/ad_agency");

app.set("view engine","ejs");
app.use(bodyParser.urlencoded({extended:true}));

// seedDB();

app.get("/ads",function(req,res){
    console.log("Home Page");
    Ad.find({},function(err,allAds){
        res.render("index",{ads:allAds});
    });
});

var obj = {};
app.post("/search/ads",function(req,res){
    var value = req.body.value;
    var key = req.body.key;
    obj[key] = value;
    Ad.find(obj,function(err,ads){
        res.render("index",{ads:ads});
    });
});

app.post("/clear",function(req,res){
    console.log(obj);
    obj = {};
    res.redirect(307,"/search/ads");
});

app.listen(3000,process.env.IP,function(){
    console.log("Server started!");
});