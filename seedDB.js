var mongoose = require("mongoose");
var Ad = require("./models/ad");

var ads = [
    {
        name:"eenadu",
        language:"telugu",
        type:"promotional",
        region:"andhra"
    },
    {
        name:"eenadu",
        language:"telugu",
        type:"promotional",
        region:"telangana"
    },
    {
        name:"sakshi",
        language:"tamil",
        type:"promotional",
        region:"tamil nadu"
    },
    {
        name:"andhra jyothi",
        language:"telugu",
        type:"classified",
        region:"andhra"
    },
    {
        name:"times of india",
        language:"english",
        type:"classified",
        region:"delhi"
    }
];

function seedDB(){
    Ad.remove({},function(err){
        if(err){
            console.log(err);
        }else{
            console.log("deleted");
        }

        ads.forEach(function(ad){
            Ad.create(ad,function(err,ad){
                if(err){
                    console.log(err);
                }else{
                    console.log("Ad created");
                }
            })
        })
    });
}

module.exports = seedDB;